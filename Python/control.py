import snap7
import os
import helper.ipscan
import time

def commandsPLC(client):
    while True:
        print("PLC commands:")
        print("1 = Hot Start")
        print("2 = Cold Start")
        print("3 = Stop")

        eingabe = int(input("Choose an option: "))
        if eingabe == 1:
            client.plc_hot_start()
            print("Hot Started")
        elif eingabe == 2:
            client.plc_cold_start()
            print("Cold Started")
        elif eingabe == 3:
            client.plc_stop()
            print("Stopped")
        else:
            print("Programm will be closed")
            client.disconnect()
            exit()


def insertIP():
    print("Enter IP address")
    print('Example: "192.168.0.1"')
    ip = input("Enter IP address: ")
    client = snap7.client.Client()
    client.connect(ip,0,0)
    if client.get_connected():
        print("Connected")
        print(client.get_cpu_info().ModuleName.decode('utf-8'))
        print(client.get_cpu_state())
#        print(client.get_plc_datetime())
        print(client.get_order_code().OrderCode.decode('utf-8'))
#        print(client.get_protection())
        commandsPLC(client)
        # for x in range(1,10):
        #     print(client.upload(x))
    else:
        print("Not Connected")
        exit()
    

print("Controll a S7 PLC")

print("Search the PLC")
print("1 = insert the IP address of the PLC")
print("2 = search the PLC")

eingabe = int(input("Choose an option: "))

if eingabe == 1:
    insertIP()
elif eingabe == 2:
    helper.ipscan.ipsearch()
    insertIP()
else:
    print("Programm will be closed")
    exit()
