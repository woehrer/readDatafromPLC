#!/usr/bin/env python3

# Import Python library
import networkscan
import socket
import csv
import os
import socket

def ipsearch():
    os.system('clear')
    print("IP Scan tools select:")
    print("1 = Scan own IP Range")
    print("2 = Scan other IP Range")
    print("3 = Scan own IP Range Netmask 16")
    print("4 = Scan other IP Range Netmask 16")        
    print("5 = Show scanned IPs")
    print("6 = Exit")

    eingabe = int(input("Choose an option: "))
    if eingabe == 1:
        scan_own()
        input("Wait for Enter")
    elif eingabe == 2:
        scan_other()
        input("Wait for Enter")
    elif eingabe == 3:
        scan_own_16()
        input("Wait for Enter")        
    elif eingabe == 4:
        scan_other_16()
        input("Wait for Enter")        
    elif eingabe == 5:
        for i in read_csv():
            print(i)
        input("Wait for Enter")
    else:
        print("Programm will be closed")
        exit()

def write_csv(iplist):
    try:
        with open('storage/scannedips.csv', 'w') as f: 
            write = csv.writer(f) 
            write.writerow(iplist) 
    except:
        print("Write error with file ")
def read_csv():
    try:
        with open('storage/scannedips.csv', newline='') as f:
            read = csv.reader(f)
            data = list(read)
            return data[0]
    except:
        print("Read error with file ")

def get_own_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

def scan(network):
    # Create the object
    my_scan = networkscan.Networkscan(network)

    # Display information
    print("Network to scan: " + str(my_scan.network))
    print("Prefix to scan: " + str(my_scan.network.prefixlen))
    print("Number of hosts to scan: " + str(my_scan.nbr_host))

    # Run the network scan
    print("Scanning hosts...")

    # Run the scan of hosts using pings
    my_scan.run()

    # Display information
    print("List of hosts found:")

    found = []
    # Display the IP address of all the hosts found
    for i in my_scan.list_of_hosts_found:
        try:
            sock = socket.socket()
            sock.settimeout(1)
            sock.connect((i, 102))
            print(i)
            found.append(i)
        except:
            print("No connection to " + i)

    # Display information
    print("Number of hosts found: " + str(my_scan.nbr_host_found))

    write_csv(found)

def scan_own():

    #Own IP address
    own_ip = get_own_ip()
    print("Eigene IP Adresse: " + own_ip)
    
    # Define the network to scan
    my_network = own_ip[:own_ip.rfind(".")] + ".0/24"

    scan(my_network)

def scan_other():
    print("Enter IP address")
    print('Example: "192.168.0.1"')
    ip = input("")

    my_network = ip[:ip.rfind(".")] + ".0/24"

    scan(my_network)

def scan_own_16():

    #Own IP address
    own_ip = get_own_ip()
    print("Eigene IP Adresse: " + own_ip)
    
    own_ip = own_ip[:own_ip.rfind(".")]
    own_ip = own_ip[:own_ip.rfind(".")] + ".0.0/16"

    # Define the network to scan
    my_network = own_ip

    scan(my_network)

def scan_other_16():
    print("Enter IP address with netmask")
    print('Example: "192.168.0.1"')
    ip = input("")
    ip = ip[:ip.rfind(".")]
    ip = ip[:ip.rfind(".")] + ".0.0/16"

    my_network = ip

    scan(my_network)