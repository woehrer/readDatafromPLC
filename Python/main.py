from ctypes.wintypes import DWORD
import snap7
import time
import helper.config
import struct

conf = helper.config.initconfig()

client = snap7.client.Client()
client.connect(conf['IP'],0,0)

while True:
    if client.get_connected():
        print("Connected")
        print(client.get_cpu_info())
        print(client.get_cpu_state())
#        print(client.get_plc_datetime())
        DB = conf['DB']
        DWORD = conf['DWORD']
        DWORD = DWORD.replace("[","")
        DWORD = DWORD.replace("]","")
        DWORD = DWORD.split(',')
        for x in DWORD:
            readb = client.db_read(int(conf['DB']),int(x),4)
            readf = struct.unpack('>f',readb)
            print(readf[0])
        time.sleep(1)
    else:
        print("Not Connected")
        time.sleep(10)
